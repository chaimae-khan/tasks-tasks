<?php

namespace Tests\Unit;

use PHPUnit\Framework\TestCase;

class MyUnitTestClass extends TestCase
{


  protected function setUp(): void
    {
        parent::setUp(); // Call the parent setUp method.
        // Custom setup logic for feature tests, if needed.
    }

    protected function tearDown(): void
    {
        // Custom teardown logic for feature tests, if needed.
        parent::tearDown(); // Call the parent tearDown method.
    }

    // Your feature test methods...
    public function test_example(): void
    {
        $this->assertTrue(true);
    }
}
